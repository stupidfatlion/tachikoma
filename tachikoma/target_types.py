"""Additional data classes to let Tacchan know what element it's looking at

.. moduleauthor:: 2501 <sshum00@gmail.com>
"""
class ID():
    def __init__(self, value):
        self.value = value

class Name():
    def __init__(self, value):
        self.value = value

class Class():
    def __init__(self, value):
        self.value = value

class Text():
    def __init__(self, value):
        self.value = value

class Url():
    def __init__(self, value):
        self.value = value

class Onclick():
    def __init__(self, value):
        self.value = value
