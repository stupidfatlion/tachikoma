"""Tachikoma will help me write crawlers and automation tools that rely on the web browser, give it a swing!

.. moduleauthor:: 2501 <sshum00@gmail.com>
"""

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException, TimeoutException, NoAlertPresentException
from target_types import ID, Name, Class, Text, Url, Onclick
from bs4 import BeautifulSoup


class Tachikoma():
    def __init__(self, proxy=None):
        self.driver = webdriver.Firefox(proxy=proxy)

    def wait_ajax(func):
        def func_wrapper(self, *args, **kwargs):
            intermediary_return_value = func(self, *args, **kwargs)
            try:
                WebDriverWait(self.driver, 30).until(lambda s: s.execute_script("return jQuery.active == 0"))
            except TimeoutException:
                pass
            finally:
                return intermediary_return_value

        return func_wrapper

    def _get_element(self, value):
        xpath = None
        if type(value) == ID:
            xpath = ".//*[@id='%s']"
        elif type(value) == Name:
            xpath = ".//*[@name='%s']"
        elif type(value) == Class:
            xpath = ".//*[@class='%s']"
        elif type(value) == Text:
            xpath = ".//*[contains(text(), '%s')]"
        elif type(value) == Url:
            xpath = ".//a[@href='%s']"
        elif type(value) == Onclick:
            xpath = './/*[@onclick="%s"]'

        if xpath:
            xpath = xpath % value.value
        else:
            raise Exception("Element type was not understood, if this is a bug, please let me know!!!")

        return self.driver.find_element_by_xpath(xpath)

    @wait_ajax
    def click_element(self, value):
        element = self._get_element(value)
        if element:
            element.click()
        else:
            raise Exception("Element could not be found to be clicked, if this is a bug, please let me know!!!")

    @wait_ajax
    def enter_text_form(self, element, text):
        element = self._get_element(element)
        if element:
            element.clear()
            element.send_keys(text)
        else:
            raise Exception("Element could not be found to be found, if this is a bug, please let me know!!!")

    def parse_tables(self):
        soup = BeautifulSoup(self.driver.page_source, "html5lib")
        tables = []
        for table in soup.find_all("table"):
            # TODO add headers if you find headers
            table_data = [[cell.string for cell in rows.find_all("td")] for rows in table.find_all("tr")]
            tables.append(table_data)
        return tables

